package standardInput;

import java.util.HashMap;
import java.util.Map;
public class Network implements Comparable<Network>{
	int nodes;
	int totalLinks;
	int totalConflicts;
	int initialConflicts;
	HashMap<String, Boolean> conflicts = new HashMap<>();
	HashMap<Integer, Integer> channels = new HashMap<>();
	HashMap<String, Integer> links = new HashMap<>();
	
	public Network() {}
	
	public Network(Network network) {
		links.putAll(network.links);
		conflicts.putAll(network.conflicts);
		channels.putAll(network.channels);
		nodes = network.nodes;
		totalLinks = network.totalLinks;
		totalConflicts = network.totalConflicts;
		initialConflicts = network.initialConflicts;
	}

	@Override
	public String toString() {
		String temp = "";
		for(Map.Entry<Integer, Integer> entry : this.channels.entrySet()) {
			temp += entry.getKey() + "=" + entry.getValue() + ", ";
		}
		return "Network [nodes=" + nodes + ", totalLinks=" + totalLinks + ", totalConflicts=" + totalConflicts + ", [" + temp + "]]";
	}

	@Override
	public int compareTo(Network o) {
		if (o.totalConflicts > this.totalConflicts){
            return -1;
        }
        if (o.totalConflicts < this.totalConflicts){
            return 1;
        }
        return 0;
	}
}
