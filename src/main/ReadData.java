package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

public class ReadData extends Network{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public ReadData(String file) {
		Scanner scanner;
		try {
			scanner = new Scanner(new File(file));
			int name;
			int x, y, r;
			while(scanner.hasNextInt()){
				name = scanner.nextInt();
			    x = scanner.nextInt();
			    y = scanner.nextInt();
			    r = scanner.nextInt();
			    this.put(new Location(x,y), new Tower(name, r));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		calculateConnection();
	}
	

	public void calculateConnection(){
		for(Map.Entry<Location, Tower> entry : this.entrySet()) {
			Location location = entry.getKey();
			Tower struct = entry.getValue();
		    for(int i = 0; i <= struct.range; i++){
		    	for(int j = 0; j <= struct.range; j++){
		    		if(i*i+j*j <= struct.range*struct.range){
		    			Location tempLocation = new Location(location.x+i, location.y+j);
			    		Tower temp = this.get(tempLocation);
			    		
			    		if(temp != null && !tempLocation.equals(location)){
			    			this.get(location).add(new Connection(tempLocation, 0));
			    		}else{
			    			tempLocation = new Location(location.x+i, location.y-j);
				    		temp = this.get(tempLocation);
				    		if(temp != null && !tempLocation.equals(location)){
				    			this.get(location).add(new Connection(tempLocation, 0));
				    		}else{
				    			tempLocation = new Location(location.x-i, location.y+j);
					    		temp = this.get(tempLocation);
					    		if(temp != null && !tempLocation.equals(location)){
					    			this.get(location).add(new Connection(tempLocation, 0));
					    		}else{
					    			tempLocation = new Location(location.x+i, location.y-j);
						    		temp = this.get(tempLocation);
						    		if(temp != null && !tempLocation.equals(location)){
						    			this.get(location).add(new Connection(tempLocation, 0));
						    		}
					    		}
				    		}
			    		}
		    		}
		    		
		    	}
		    }
		}
	}
}
