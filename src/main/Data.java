package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Data extends HashMap<Location, Struct>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Data(String file){
		Scanner scanner;
		try {
			scanner = new Scanner(new File(file));
			int name;
			int x, y, r;
			while(scanner.hasNextInt()){
				name = scanner.nextInt();
			    x = scanner.nextInt();
			    y = scanner.nextInt();
			    r = scanner.nextInt();
			    this.put(new Location(x,y), new Struct(name, r));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		calculateConnection();
	}
	
	public void calculateConnection(){
		for(Map.Entry<Location, Struct> entry : this.entrySet()) {
			Location location = entry.getKey();
			Struct struct = entry.getValue();
		    for(int i = 0; i <= struct.range; i++){
		    	for(int j = 0; j <= struct.range; j++){
		    		if(i*i+j*j <= struct.range*struct.range){
		    			Location tempLocation = new Location(location.x+i, location.y+j);
			    		Struct temp = this.get(tempLocation);
			    		
			    		if(temp != null && !tempLocation.equals(location)){
			    			this.get(location).add(temp.name);
			    		}else{
			    			tempLocation = new Location(location.x+i, location.y-j);
				    		temp = this.get(tempLocation);
				    		if(temp != null && !tempLocation.equals(location)){
				    			this.get(location).add(temp.name);
				    		}else{
				    			tempLocation = new Location(location.x-i, location.y+j);
					    		temp = this.get(tempLocation);
					    		if(temp != null && !tempLocation.equals(location)){
					    			this.get(location).add(temp.name);
					    		}else{
					    			tempLocation = new Location(location.x+i, location.y-j);
						    		temp = this.get(tempLocation);
						    		if(temp != null && !tempLocation.equals(location)){
						    			this.get(location).add(temp.name);
						    		}
					    		}
				    		}
			    		}
		    		}
		    		
		    	}
		    }
		}
	}

	@Override
	public String toString() {
		String temp = "";
		for(Map.Entry<Location, Struct> entry : this.entrySet()) {
		    temp += "\n["+entry.getKey() +"="+ entry.getValue() + "], ";
		}
		return "Data ["+temp+"]";
	}
}

