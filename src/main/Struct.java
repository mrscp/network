package main;

import java.util.ArrayList;

public class Struct extends ArrayList<Integer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8253853728885632601L;
	
	int name, range;
	public Struct(int name, int range){
		this.name = name;
		this.range = range;
	}
	
	@Override
	public String toString(){
		String temp = "";
		for(Integer connection: this){
			temp += connection + ", ";
		}
		return "Struct [name=" + name + ", range=" + range + ", connections=["+temp+"]]";
	}
}
