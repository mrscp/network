package newStandardInputModifiedGA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

public class Population {
	ArrayList<Network> population = new ArrayList<>();
	HashMap<Integer, ArrayList<Integer>> conflictSchema = new HashMap<Integer, ArrayList<Integer>>();
	HashMap<Integer, ArrayList<Integer>> linkSchema = new HashMap<Integer, ArrayList<Integer>>();
	HashMap<String, Integer> links = new HashMap<>();
	
	int nodes;
	int initialConflicts;
	int totalLinks;
	
	Network best;
	int populationSize;
	int availableChannel;
	int availableUniqueChannel;
	
	public Population(ReadData data, 
			int availableChannel, 
			int availableUniqueChannel, 
			int populationSize) {
		this.availableChannel = availableChannel;
		this.availableUniqueChannel = availableUniqueChannel;
		this.populationSize = populationSize;
		best = data.network;
		this.conflictSchema = data.conflictSchema;
		this.linkSchema = data.linkSchema;
		this.links = data.links;
		nodes = data.nodes;
		initialConflicts = data.initialConflicts;
		totalLinks = data.totalLinks;
	}

	public void startGeneration(){
		for(int i = 0; i < populationSize; i++){
			population.add(i, randomNetwork(best));
		}
	}
	
	public HashMap<String, Boolean> randomUniqueChannel(){
		HashMap<String, Boolean> unique = new HashMap<>();
		for(Map.Entry<Integer, ArrayList<Integer>> entry: linkSchema.entrySet()){
			HashSet<Integer> temp = new HashSet<>();
			while(temp.size() < availableUniqueChannel && temp.size() < entry.getValue().size()){
				int r = rand.nextInt(entry.getValue().size());
				temp.add(r);
			}

			for(Integer i: temp){
				String key = entry.getKey() +" "+ entry.getValue().get(i);
				unique.put(key, true);
			}
		}
		return unique;
	}
	
	public String getLinkKey(int x, int y){
		return y > x?x+" "+y:y+" "+x;
	}
	
	public Network randomNetwork(Network network){
		Network temp = new Network(network);
		temp.unique = randomUniqueChannel();
		for(Map.Entry<Integer, ArrayList<Integer>> entry: this.linkSchema.entrySet()){
			ArrayList<Integer> uniqueList = new ArrayList<>();
			while(uniqueList.size() < availableUniqueChannel && uniqueList.size() < entry.getValue().size()){
				int channel = getChannel();
				if(!uniqueList.contains(channel)){
					uniqueList.add(channel);
				}
			}
			int index = 0;
			for(Integer target: entry.getValue()){
				int key = links.get(getLinkKey(entry.getKey(), target));
				if(temp.unique.containsKey(entry.getKey() +" "+ target)){
					temp.channels.put(key, uniqueList.get(index));
					index++;
				}else{
					int channel = getChannel();
					while(uniqueList.contains(channel)){
						channel = getChannel();
					}
					temp.channels.put(key, channel);
				}
			}
		}
		/*for(int i = 0; i < totalLinks; i++){
			temp.channels.put(i, getChannel());
		}*/
		temp = new Fitness().calculate(temp);
		temp.age = 0;
		return temp;
	}

	Random rand = new Random();
	public Network selectParent(){
		int index = rand.nextInt(this.populationSize);
		double probability = Math.random();
		
		double fitness = 1 - (double)(population.get(index).totalConflicts) / ((double)initialConflicts);
		if(probability < fitness){
			return population.get(index);
		}
		return selectParent();
	}
	
	public Network normalBirth(Network parent1, Network parent2){
		Network child1 = new Network(parent1);
		Network child2 = new Network(parent1);
		int firstPoint = rand.nextInt(totalLinks);
		int secondPoint = firstPoint + rand.nextInt(totalLinks-firstPoint);
		for(int j = 0; j < totalLinks; j++){
			if(j <= firstPoint || j >= secondPoint){
				child1.channels.put(j, parent1.channels.get(j));
				child2.channels.put(j, parent2.channels.get(j));
			}else{
				child2.channels.put(j, parent1.channels.get(j));
				child1.channels.put(j, parent2.channels.get(j));
			}
		}
		/*
		child1.channels = new HashMap<>();
		child2.channels = new HashMap<>();
		for(Map.Entry<String, Boolean> entry : parent1.conflicts.entrySet()) {
			String[] key = entry.getKey().split(" ");
			if(entry.getValue()){
				child1.channels.put(Integer.parseInt(key[0]), parent2.channels.get(Integer.parseInt(key[0])));
				child2.channels.put(Integer.parseInt(key[0]), parent1.channels.get(Integer.parseInt(key[0])));
			
				child1.channels.put(Integer.parseInt(key[1]), parent2.channels.get(Integer.parseInt(key[1])));
				child2.channels.put(Integer.parseInt(key[1]), parent1.channels.get(Integer.parseInt(key[1])));
			}else{
				child1.channels.put(Integer.parseInt(key[0]), parent1.channels.get(Integer.parseInt(key[0])));
				child2.channels.put(Integer.parseInt(key[0]), parent2.channels.get(Integer.parseInt(key[0])));
			}
		}*/
		child1 = new Fitness().calculate(child1);
		child2 = new Fitness().calculate(child2);
		child1.age = 0;
		child2.age = 0;
		if(child1.totalConflicts < child2.totalConflicts){
			return child1;
		}
		return child2;
	}
	
	public void crossOver(){
		int count = 0;
		this.best = population.get(0);
		for(int i = 0; i < populationSize; i++){
			double probability = Math.random();
			
			double fitness = 1 - (double)(population.get(i).totalConflicts) / ((double)initialConflicts);
			
			if(probability > fitness*0.50 && population.get(i).totalConflicts != best.totalConflicts){
				count++;
				Network parent1 = selectParent();
				Network parent2 = selectParent();
				Network child = normalBirth(parent1, parent2);
				child.age++;
				population.set(i,child);
			}else{
				population.get(i).age++;
			}
			if(this.best.totalConflicts > population.get(i).totalConflicts){
				this.best = population.get(i);
			}
		}
		System.out.println("crosses "+count);
	}
	
	public int getChannel(){
		return (int)(Math.random()*availableChannel)%availableChannel;
	}
	
	public Network randomNeighbor(Network network){
		Network temp = new Network(network);
		int i = (int)(Math.random()*totalLinks)%totalLinks;
		temp.channels.put(i, getChannel());
		
		temp = new Fitness().calculate(temp);
		return temp;
	}
	
	public Network curingNeighbor(Network network){
		Network temp = new Network(network);
		
		for(Map.Entry<String, Boolean> entry : temp.conflicts.entrySet()) {
			if(entry.getValue()){
				String[] key = entry.getKey().split(" ");
				int x = Integer.parseInt(key[0]);
				int y = Integer.parseInt(key[1]);
				temp.channels.put(x, getChannel());
				temp.channels.put(y, getChannel());
			}
		}
		
		temp = new Fitness().calculate(temp);
		return temp;
	}
	
	public void mutation(){
		this.best = population.get(0);
		int count = 0;
		for(int i = 0; i < populationSize; i++){
			
			double probability = Math.random();
			
			double fitness = 1 - (double)(population.get(i).totalConflicts) / ((double)initialConflicts);
			
			if(probability > fitness*0.50 && population.get(i).totalConflicts != best.totalConflicts){
				count++;
				population.set(i,randomNeighbor(population.get(i)));
				
			}else{
				population.get(i).age++;
			}
			if(this.best.totalConflicts > population.get(i).totalConflicts){
				this.best = population.get(i);
			}
		}
		System.out.println("mutations: " + count);
	}
	
	public void deathRace(){
		for(int i = 0; i < populationSize; i++){
			if(population.get(i).age > 10){
				population.set(i,randomNetwork(population.get(i)));
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}
		}
	}
	
	public void twinRemoval(){
		ArrayList<Network> tempPop = new ArrayList<>();
		for(int i = 0; i < populationSize; i++){
			if(tempPop.contains(population.get(i))){
				tempPop.add(randomNetwork(population.get(i)));
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}else{
				tempPop.add(population.get(i));
			}
		}
		population = new ArrayList<>(tempPop);
	}
	
	
	public Network cure(Network network){
		System.out.println("Curing: "+network);
		for(Map.Entry<String, Boolean> entry : network.conflicts.entrySet()) {
			if(entry.getValue() == true){
				String[] key = entry.getKey().split(" ");
				network.channels.put(Integer.parseInt(key[0]), getChannel());
				network.channels.put(Integer.parseInt(key[1]), getChannel());
			}
		}
		network = new Fitness().calculate(network);
		System.out.println("Cured: "+network);
		return network;
	}
	
	public void treatment(){
		ArrayList<Network> tempPop = new ArrayList<>();
		
		for(int i = 0; i < populationSize; i++){
			if(population.get(i).totalConflicts < 900){
				tempPop.add(cure(population.get(i)));
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}else{
				tempPop.add(population.get(i));
			}
		}
		population = new ArrayList<>(tempPop);
	}
	
	@Override
	public String toString() {
		String temp = "";
		for(Network network: population){
			temp += network + ",\n";
		}
		return "Population [" + temp + "]";
	}
}
