package newStandardInputModifiedGA;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ModifiedGA {
	int conflictsWillBeConsider = 500;
	public ModifiedGA(ReadData data, 
			int availableChannel, 
			int availableUniqueChannel, 
			int populationSize){
		
		long startTime = System.currentTimeMillis();
		
		Population pop = new Population(data, 
				availableChannel, 
				availableUniqueChannel, 
				populationSize);
		pop.startGeneration();
		int generation = 0;
		while(true){
			System.out.println(pop);
			if(pop.best.totalConflicts < conflictsWillBeConsider){
				System.out.println(pop.best);
				break;
			}
			System.out.println("Generation " + generation + ": " + pop.best);
			//System.out.println(pop);
			pop.crossOver();
			pop.mutation();
			pop.deathRace();
			pop.twinRemoval();
			//pop.treatment();
			
			generation++;
			
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println(getTime(totalTime));
		}
		
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(getTime(totalTime));
	}

	public String getTime(long totalTime){
		if(totalTime > 100)
			return ("Time: " + String.format("%d min, %d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(totalTime),
			    TimeUnit.MILLISECONDS.toSeconds(totalTime) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))
			));
		else{
			return ("Time: " + totalTime + " ms");
		}
	}
	
	public static void main(String args[]){
		ReadData data = new ReadData("topology_500x500_tx120_cs200.txt");
		data.network = new Fitness().calculate(data.network);
		for(Map.Entry<Integer, ArrayList<Integer>> entry: data.linkSchema.entrySet()){
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
		int availableChannel = 12;
		int availableUniqueChannel = 3;
		int populationSize = 100;
		new ModifiedGA(data, availableChannel, availableUniqueChannel, populationSize);
	}
}
