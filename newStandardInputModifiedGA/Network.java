package newStandardInputModifiedGA;

import java.util.HashMap;
import java.util.Map;
public class Network implements Comparable<Network>{
	int totalConflicts;
	int age;
	HashMap<String, Boolean> conflicts = new HashMap<>();
	HashMap<Integer, Integer> channels = new HashMap<>();
	HashMap<String, Boolean> unique = new HashMap<>();
	
	public Network() {
		age = 1;
	}
	
	public Network(Network network) {
		conflicts.putAll(network.conflicts);
		channels.putAll(network.channels);
		totalConflicts = network.totalConflicts;
		age = network.age;
		unique = network.unique;
	}

	@Override
	public String toString() {
		String temp = "";
		for(Map.Entry<Integer, Integer> entry : this.channels.entrySet()) {
			temp += entry.getKey() + "=" + entry.getValue() + ", ";
		}
		return "Network [totalConflicts=" + totalConflicts+ ", age=" + age + ", [" + temp + "]]";
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + totalConflicts;
		for(int i = 0; i < channels.size(); i++){
			result = prime * (result + channels.get(i));
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Network o = (Network)obj;
		for(int i = 0; i < channels.size(); i++){
			if(o.channels.get(i) != this.channels.get(i)){
				return false;
			}
		}
		return true;
	}

	@Override
	public int compareTo(Network o) {
		if (o.totalConflicts > this.totalConflicts){
            return -1;
        }
        if (o.totalConflicts < this.totalConflicts){
            return 1;
        }
        return 0;
	}
}
