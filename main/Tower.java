package main;

import java.util.ArrayList;

public class Tower extends ArrayList<Connection> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8253853728885632601L;
	
	int name, range;
	
	public Tower(int name, int range){
		this.name = name;
		this.range = range;
	}
	
	@Override
	public String toString(){
		String temp = "";
		for(Connection connection: this){
			temp += connection + ", ";
		}
		return "Tower [name=" + name + ", range=" + range + ", connections=["+temp+"]]";
	}
}
