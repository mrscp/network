package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Network extends HashMap<Location, Tower>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int age = 0;
	ArrayList<Boolean> error = new ArrayList<>();
	double fitness = 0.0;
	
	public Network() {}
	
	public Network(Network network) {
		this.putAll(network);
		this.age = network.age;
		this.fitness = network.fitness;
		this.error.addAll(network.error);
	}
	
	@Override
	public String toString() {
		String temp = "";
		for(Map.Entry<Location, Tower> entry : this.entrySet()) {
		    temp += "\n["+entry.getKey() +"="+ entry.getValue() + "], ";
		}
		return "Data ["+temp+"]";
	}
}

