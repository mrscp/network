package main;

public class Location {
	int x, y;
	public Location(int x, int y){
		this.x = x;
		this.y = y;
	}
	@Override
	public String toString() {
		return "Location [x=" + x + ", y=" + y + "]";
	}
	
	@Override
    public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + x;
		hashCode = 31 * hashCode + y;
		return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
       if (!(obj instanceof Location))
            return false;
        if (obj == this)
            return true;

        Location o = (Location) obj;
        return this.x == o.x && this.y == o.y;
    }
}
