package main;

public class Connection {
	Location connectedTo;
	int color;

	public Connection(Location connectedTo, int color) {
		this.connectedTo = connectedTo;
		this.color = color;
	}

	@Override
	public String toString() {
		return "Connection [connectedTo=" + connectedTo + ", color=" + color + "]";
	}
}
