package main;

import java.util.ArrayList;
import java.util.Map;


public class Fitness {
	public int calculate(Network data){
		int count = 0;
		for(Map.Entry<Location, Tower> entry : data.entrySet()) {
		    //temp += "\n["+entry.getKey() +"="+ entry.getValue() + "], ";
			//System.out.println(entry.getValue());
			ArrayList<Integer> colors = new ArrayList<>();
			//System.out.println(entry.getKey());
			for(Connection connection: (Tower) entry.getValue()){
				if(colors.contains(connection.color)){
					count++;
				}else{
					colors.add(connection.color);
				}
			}
		}
		return count;
	}
}
