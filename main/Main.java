package main;

public class Main {
	public static void main(String...args) {
		Network data = new ReadData("data.txt");
		Network network = new Network(data);
		System.out.println(network);
		Fitness fitness = new Fitness();
		System.out.println(fitness.calculate(data));
	}
}
