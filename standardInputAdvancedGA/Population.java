package standardInputAdvancedGA;

import java.util.ArrayList;
import java.util.Random;

public class Population {
	ArrayList<Network> population = new ArrayList<>();
	Network best;
	int populationSize;
	int availableChannel;
	int availableUniqueChannel;
	
	public Population(Network network, int availableChannel, int availableUniqueChannel, int populationSize) {
		this.availableChannel = availableChannel;
		this.availableUniqueChannel = availableUniqueChannel;
		this.populationSize = populationSize;
		best = network;
	}

	public void startGeneration(){
		for(int i = 0; i < populationSize; i++){
			population.add(i, randomNetwork(best));
		}
	}
	
	public Network randomNetwork(Network network){
		Network temp = new Network(network);
		for(int i = 0; i < network.totalLinks; i++){
			temp.channels.put(i, (int)(Math.random()*availableChannel)%availableChannel);
		}
		temp = new Fitness().calculate(temp);
		temp.age = 0;
		return temp;
	}

	Random rand = new Random();
	public Network selectParent(){
		int index = rand.nextInt(this.populationSize);
		double probability = Math.random();
		
		double fitness = 1 - (double)(population.get(index).totalConflicts) / (double)(population.get(index).initialConflicts);
		
		if(probability < fitness){
			return population.get(index);
		}
		return selectParent();
	}
	
	public Network normalBirth(Network parent1, Network parent2){
		Network child1 = new Network(parent1);
		Network child2 = new Network(parent1);
		int firstPoint = rand.nextInt(parent1.totalLinks);
		int secondPoint = firstPoint + rand.nextInt(parent1.totalLinks-firstPoint);
		for(int j = 0; j < parent1.totalLinks; j++){
			if(j <= firstPoint || j >= secondPoint){
				child1.channels.put(j, parent1.channels.get(j));
				child2.channels.put(j, parent2.channels.get(j));
			}else{
				child2.channels.put(j, parent1.channels.get(j));
				child1.channels.put(j, parent2.channels.get(j));
			}
		}
		
		child1 = new Fitness().calculate(child1);
		child2 = new Fitness().calculate(child2);
		child1.age = 0;
		child2.age = 0;
		if(child1.totalConflicts < child2.totalConflicts){
			return child1;
		}
		return child2;
	}
	
	public void crossOver(){
		for(int i = 0; i < populationSize; i++){
			double probability = Math.random();
			
			double fitness = 1 - (double)(population.get(i).totalConflicts) / (double)(population.get(i).initialConflicts);
			//System.out.println(probability + " " + fitness);
			if(probability > fitness*0.5){
				//System.out.println("Yes");
				Network parent1 = selectParent();
				Network parent2 = selectParent();
				Network child = normalBirth(parent1, parent2);
				child.age++;
				population.set(i,child);
			}else{
				population.get(i).age++;
			}
		}
	}
	
	public Network randomNeighbor(Network network){
		Network temp = new Network(network);
		int i = (int)(Math.random()*temp.totalLinks)%temp.totalLinks;
		temp.channels.put(i, (int)(Math.random()*availableChannel)%availableChannel);
		
		temp = new Fitness().calculate(temp);
		return temp;
	}
	
	public void mutation(){
		this.best = population.get(0);
		for(int i = 0; i < populationSize; i++){
			population.set(i,randomNeighbor(population.get(i)));
			if(this.best.totalConflicts > population.get(i).totalConflicts){
				this.best = population.get(i);
			}
		}
	}
	
	public void deathRace(){
		this.best = population.get(0);
		for(int i = 0; i < populationSize; i++){
			if(population.get(i).age > 10){
				population.set(i,randomNetwork(population.get(i)));
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}
		}
	}
	
	public void twinRemoval(){
		this.best = population.get(0);
		ArrayList<Network> tempPop = new ArrayList<>();
		
		for(int i = 0; i < populationSize; i++){
			if(tempPop.contains(population.get(i))){
				System.out.println("removed");
				tempPop.add(randomNetwork(population.get(i)));
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}else{
				tempPop.add(population.get(i));
			}
		}
		population = new ArrayList<>(tempPop);
	}
	
	@Override
	public String toString() {
		String temp = "";
		for(Network network: population){
			temp += network + ",\n";
		}
		return "Population [" + temp + "]";
	}
}
