package standardInputAdvancedGA;

import java.util.HashMap;
import java.util.Map;
public class Network implements Comparable<Network>{
	int nodes;
	int totalLinks;
	int totalConflicts;
	int initialConflicts;
	int age;
	HashMap<String, Boolean> conflicts = new HashMap<>();
	HashMap<Integer, Integer> channels = new HashMap<>();
	HashMap<String, Integer> links = new HashMap<>();
	
	public Network() {
		age = 1;
	}
	
	public Network(Network network) {
		links.putAll(network.links);
		conflicts.putAll(network.conflicts);
		channels.putAll(network.channels);
		nodes = network.nodes;
		totalLinks = network.totalLinks;
		totalConflicts = network.totalConflicts;
		initialConflicts = network.initialConflicts;
		age = network.age;
	}

	@Override
	public String toString() {
		String temp = "";
		for(Map.Entry<Integer, Integer> entry : this.channels.entrySet()) {
			temp += entry.getKey() + "=" + entry.getValue() + ", ";
		}
		return "Network [nodes=" + nodes + ", totalLinks=" + totalLinks + ", totalConflicts=" + totalConflicts+ ", age=" + age + ", [" + temp + "]]";
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + initialConflicts;
		result = prime * result + nodes;
		result = prime * result + totalConflicts;
		result = prime * result + totalLinks;
		for(int i = 0; i < totalLinks; i++){
			result = prime * (result + channels.get(i));
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Network o = (Network)obj;
		for(int i = 0; i < totalLinks; i++){
			if(o.channels.get(i) != this.channels.get(i)){
				return false;
			}
		}
		return true;
	}

	@Override
	public int compareTo(Network o) {
		if (o.totalConflicts > this.totalConflicts){
            return -1;
        }
        if (o.totalConflicts < this.totalConflicts){
            return 1;
        }
        return 0;
	}
}
