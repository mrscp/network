package standardInput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadData extends Network {
	public ReadData(String file) {
		Scanner scanner;
		try {
			scanner = new Scanner(new File(file));
			scanner.nextLine();
			scanner.next();
			this.nodes = scanner.nextInt();
			scanner.next();
			this.totalLinks = scanner.nextInt();
			scanner.next();
			this.totalConflicts = scanner.nextInt();
			this.initialConflicts = this.totalConflicts;
			scanner.next();
			scanner.next();
			int s, a, b;
			for(int i = 0; i < totalLinks; i++){
				s = scanner.nextInt();
				a = scanner.nextInt();
				b = scanner.nextInt();
				this.links.put(a+" "+b, s);
				this.channels.put(s, 0);
			}
			
			scanner.next();
			scanner.next();
			for(int i = 0; i < totalConflicts; i++){
				s = scanner.nextInt();
				a = scanner.nextInt();
				b = scanner.nextInt();
				this.conflicts.put(a+" "+b, true);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
