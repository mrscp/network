package standardInput;

import java.util.ArrayList;
import java.util.Random;

public class Population {
	ArrayList<Network> population = new ArrayList<>();
	Network best;
	int populationSize;
	int availableChannel;
	int availableUniqueChannel;
	
	public Population(Network network, int availableChannel, int availableUniqueChannel, int populationSize) {
		this.availableChannel = availableChannel;
		this.availableUniqueChannel = availableUniqueChannel;
		this.populationSize = populationSize;
		best = network;
	}

	public void startGeneration(){
		for(int i = 0; i < populationSize; i++){
			population.add(i, generateRandomNetwork(best));
		}
	}
	
	public Network generateRandomNetwork(Network network){
		Network temp = new Network(network);
		for(int i = 0; i < network.totalLinks; i++){
			temp.channels.put(i, (int)(Math.random()*availableChannel)%availableChannel);
		}
		temp = new Fitness().calculate(temp);
		return temp;
	}

	Random rand = new Random();
	public Network selectParent(){
		int index = rand.nextInt(this.populationSize);
		double probability = Math.random();
		
		double fitness = 1 - (double)(population.get(index).totalConflicts) / (double)(population.get(index).initialConflicts);
		
		if(probability < fitness){
			return population.get(index);
		}
		//System.out.println(population.get(index).initialConflicts + " " + population.get(index).totalConflicts + " " + probability + " " + fitness);
		return selectParent();
	}
	
	public Network normalBirth(Network parent1, Network parent2){
		ArrayList<Network> temp = new ArrayList<>();
		temp.add(parent1);
		temp.add(parent2);
		int firstPoint = rand.nextInt(parent1.totalLinks);
		int secondPoint = firstPoint + rand.nextInt(parent1.totalLinks-firstPoint);
		for(int j = 0; j < parent1.totalLinks; j++){
			if(j <= firstPoint || j >= secondPoint){
				temp.get(0).channels.put(j, parent1.channels.get(j));
				temp.get(1).channels.put(j, parent2.channels.get(j));
			}else{
				temp.get(1).channels.put(j, parent1.channels.get(j));
				temp.get(0).channels.put(j, parent2.channels.get(j));
			}
		}
		temp.set(0, new Fitness().calculate(temp.get(0)));
		temp.set(1, new Fitness().calculate(temp.get(1)));
		if(temp.get(0).totalConflicts < temp.get(1).totalConflicts){
			return temp.get(0);
		}
		return temp.get(0);
	}
	
	public void crossOver(){
		for(int i = 0; i < populationSize; i++){
			double probability = Math.random();
			
			double fitness = 1 - (double)(population.get(i).totalConflicts) / (double)(population.get(i).initialConflicts);
			//System.out.println(probability + " " + fitness);
			if(probability > fitness*0.5){
				//System.out.println("Yes");
				Network parent1 = selectParent();
				Network parent2 = selectParent();
				Network child = normalBirth(parent1, parent2);
				population.set(i,child);
			}
		}
	}
	
	public Network randomNeighbor(Network network){
		Network temp = new Network(network);
		int i = (int)(Math.random()*temp.totalLinks)%temp.totalLinks;
		temp.channels.put(i, (int)(Math.random()*availableChannel)%availableChannel);
		
		temp = new Fitness().calculate(temp);
		return temp;
	}
	
	public void mutation(){
		this.best = population.get(0);
		for(int i = 0; i < populationSize; i++){
			population.set(i,randomNeighbor(population.get(i)));
			if(this.best.totalConflicts > population.get(i).totalConflicts){
				this.best = population.get(i);
			}
		}
	}
	
	@Override
	public String toString() {
		String temp = "";
		for(Network network: population){
			temp += network + ",\n";
		}
		return "Population [" + temp + "]";
	}
}
