package standardInputModifiedGA;

import java.util.concurrent.TimeUnit;

public class ModifiedGA {
	int conflictsWillBeConsider = 500;
	public ModifiedGA(Network network, int availableChannel, int availableUniqueChannel, int populationSize){
		
		long startTime = System.currentTimeMillis();
		
		Population pop = new Population(network, availableChannel, availableUniqueChannel, populationSize);
		pop.startGeneration();
		int generation = 0;
		while(true){
			if(pop.best.totalConflicts < conflictsWillBeConsider){
				System.out.println(pop.best);
				break;
			}
			System.out.println("Generation " + generation + ": " + pop.best);
			//System.out.println(pop);
			pop.crossOver();
			pop.mutation();
			pop.deathRace();
			pop.twinRemoval();
			//pop.treatment();
			
			generation++;
			
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println(getTime(totalTime));
		}
		
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(getTime(totalTime));
	}

	public String getTime(long totalTime){
		if(totalTime > 100)
			return ("Time: " + String.format("%d min, %d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(totalTime),
			    TimeUnit.MILLISECONDS.toSeconds(totalTime) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))
			));
		else{
			return ("Time: " + totalTime + " ms");
		}
	}
	
	public static void main(String args[]){
		Network network = new ReadData("topology_500x500_tx120_cs200.txt");
		network = new Fitness().calculate(network);
		int availableChannel = 12;
		int availableUniqueChannel = 3;
		int populationSize = 100;
		new ModifiedGA(network, availableChannel, availableUniqueChannel, populationSize);
		
	}
	
}
