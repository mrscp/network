package standardInputModifiedGA;

import java.util.HashMap;
import java.util.Map;

public class Fitness {
	public Network calculate(Network network){
		int count = 0;
		HashMap<String, Boolean> tempConflicts = new HashMap<>();
		tempConflicts.putAll(network.conflicts);
		for(Map.Entry<String, Boolean> entry : network.conflicts.entrySet()) {
			String[] link = entry.getKey().split(" ");
			if(network.channels.get(Integer.parseInt(link[0])) != network.channels.get(Integer.parseInt(link[1]))){
				tempConflicts.put(entry.getKey(), false);
			}else{
				tempConflicts.put(entry.getKey(), true);
				count++;
			}
		}
		network.totalConflicts = count;
		network.conflicts = tempConflicts;
		return network;
	}
}
