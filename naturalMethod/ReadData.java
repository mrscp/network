package naturalMethod;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class ReadData{
	Network network = new Network();
	HashMap<Integer, ArrayList<Integer>> conflictSchema = new HashMap<Integer, ArrayList<Integer>>();
	HashMap<Integer, ArrayList<Integer>> linkSchema = new HashMap<Integer, ArrayList<Integer>>();
	HashMap<String, Integer> links = new HashMap<>();
	
	int nodes;
	int initialConflicts;
	int totalLinks;
	
	public ReadData(String file) {
		Scanner scanner;
		try {
			scanner = new Scanner(new File(file));
			scanner.nextLine();
			scanner.next();
			nodes = scanner.nextInt();
			scanner.next();
			totalLinks = scanner.nextInt();
			scanner.next();
			network.totalConflicts = scanner.nextInt();
			initialConflicts = network.totalConflicts;
			scanner.next();
			scanner.next();
			int s, a, b;
			for(int i = 0; i < totalLinks; i++){
				s = scanner.nextInt();
				a = scanner.nextInt();
				b = scanner.nextInt();
				this.links.put(a+" "+b, s);
				network.channels.put(s, 0);
				if(linkSchema.containsKey(a)){
					linkSchema.get(a).add(b);
				}else{
					linkSchema.put(a, new ArrayList<Integer>());
					linkSchema.get(a).add(b);
				}
				if(linkSchema.containsKey(b)){
					linkSchema.get(b).add(a);
				}else{
					linkSchema.put(b, new ArrayList<Integer>());
					linkSchema.get(b).add(a);
				}
			}
			
			scanner.next();
			scanner.next();
			for(int i = 0; i < network.totalConflicts; i++){
				s = scanner.nextInt();
				a = scanner.nextInt();
				b = scanner.nextInt();
				network.conflicts.put(a+" "+b, true);
				
				if(conflictSchema.containsKey(a)){
					conflictSchema.get(a).add(b);
				}else{
					conflictSchema.put(a, new ArrayList<Integer>());
					conflictSchema.get(a).add(b);
				}
				if(conflictSchema.containsKey(b)){
					conflictSchema.get(b).add(a);
				}else{
					conflictSchema.put(b, new ArrayList<Integer>());
					conflictSchema.get(b).add(a);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
