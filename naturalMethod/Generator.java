package naturalMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Generator {
	ArrayList<Network> population = new ArrayList<>();
	HashMap<Integer, ArrayList<Integer>> conflictSchema = new HashMap<Integer, ArrayList<Integer>>();
	HashMap<Integer, ArrayList<Integer>> linkSchema = new HashMap<Integer, ArrayList<Integer>>();
	HashMap<String, Integer> links = new HashMap<>();
	int nodes;
	int initialConflicts;
	int totalLinks;
	int populationSize;
	int availableChannel;
	int availableUniqueChannel;
	
	public int getChannel(){
		return (int)(Math.random()*availableChannel)%availableChannel;
	}
	
	Random rand = new Random();

	public String generateLinkKey(int x, int y){
		return y > x?x+" "+y:y+" "+x;
	}
	
	public HashMap<String, Boolean> randomUniqueChannel(){
		HashMap<String, Boolean> unique = new HashMap<>();
		for(Map.Entry<Integer, ArrayList<Integer>> entry: linkSchema.entrySet()){
			HashSet<Integer> temp = new HashSet<>();
			while(temp.size() < availableUniqueChannel && temp.size() < entry.getValue().size()){
				int r = rand.nextInt(entry.getValue().size());
				temp.add(r);
			}

			for(Integer i: temp){
				String key = entry.getKey() +" "+ entry.getValue().get(i);
				unique.put(key, true);
			}
		}
		return unique;
	}
	
	public Network randomNetwork(Network network){
		Network temp = new Network(network);
		temp.unique = randomUniqueChannel();
		for(Map.Entry<Integer, ArrayList<Integer>> entry: this.linkSchema.entrySet()){
			ArrayList<Integer> uniqueList = new ArrayList<>();
			while(uniqueList.size() < availableUniqueChannel && uniqueList.size() < entry.getValue().size()){
				int channel = getChannel();
				if(!uniqueList.contains(channel)){
					uniqueList.add(channel);
				}
			}
			int index = 0;
			for(Integer target: entry.getValue()){
				int key = links.get(generateLinkKey(entry.getKey(), target));
				if(temp.unique.containsKey(entry.getKey() +" "+ target)){
					temp.channels.put(key, uniqueList.get(index));
					index++;
				}else{
					int channel = getChannel();
					while(uniqueList.contains(channel)){
						channel = getChannel();
					}
					temp.channels.put(key, channel);
				}
			}
		}
		temp = new Fitness().calculate(temp);
		temp.age = 0;
		return temp;
	}
	
	public String getAConflictedKey(Network temp){
		List<String> keys = new ArrayList<String>(temp.conflicts.keySet());
		String key = keys.get(rand.nextInt(keys.size()));
		while(!temp.conflicts.get(key)){
			key = keys.get(rand.nextInt(keys.size()));
		}
		return key;
	}
	
	public Network curingNeighbor(Network network){
		Network temp = new Network(network);
		String key = getAConflictedKey(temp);
		//System.out.println(key + " " + temp.conflicts.get(key));
		String[] keys = key.split(" ");
		int x = Integer.parseInt(keys[0]);
		int y = Integer.parseInt(keys[1]);
		temp.channels.put(x, getChannel());
		temp.channels.put(y, getChannel());
		
		temp = new Fitness().calculate(temp);
		return temp;
	}
	
	public int selectAPerson(){
		int index = rand.nextInt(this.populationSize);
		double probability = Math.random();
		
		double fitness = 1-(double)(population.get(index).totalConflicts) / ((double)initialConflicts);
		//System.out.print(fitness);
		fitness = Math.pow(fitness, 2);
		//System.out.println(" " + fitness + " " + probability + " " + population.get(index).totalConflicts);
		if(probability < fitness){
			return index;
		}
		return selectAPerson();
	}
	
	public boolean acceptance(int x, int y){
		double probability = Math.random();			
		double fitness = 1-(double)x/y;
		fitness = Math.pow((1-(fitness/2)), 2)-0.7;
		return probability < fitness;
	}
	
	public Network[] collaborate(Network network1, Network network2){
		Network smart1 = new Network(network1);
		Network smart2 = new Network(network2);
		
		String key = getAConflictedKey(smart1);

		String[] keys = key.split(" ");
		int x = Integer.parseInt(keys[0]);
		int y = Integer.parseInt(keys[1]);
		
		smart1.channels.put(x, network2.channels.get(x));
		smart1.channels.put(y, network2.channels.get(y));
		
		smart1 = new Fitness().calculate(smart1);
		
		smart2.channels.put(x, network1.channels.get(x));
		smart2.channels.put(y, network1.channels.get(y));
		
		smart2 = new Fitness().calculate(smart2);
		return new Network[]{
				acceptance(network1.totalConflicts, smart1.totalConflicts)?smart1:network1,
				acceptance(network2.totalConflicts, smart2.totalConflicts)?smart2:network2
			};
	}

	public Network randomNeighbor(Network network){
		Network temp = new Network(network);
		int i = (int)(Math.random()*totalLinks)%totalLinks;
		temp.channels.put(i, getChannel());
		
		temp = new Fitness().calculate(temp);
		return temp;
	}

	public Network normalBirth(Network parent1, Network parent2){
		Network child1 = new Network(parent1);
		Network child2 = new Network(parent1);
		int firstPoint = rand.nextInt(totalLinks);
		int secondPoint = firstPoint + rand.nextInt(totalLinks-firstPoint);
		for(int j = 0; j < totalLinks; j++){
			if(j <= firstPoint || j >= secondPoint){
				child1.channels.put(j, parent1.channels.get(j));
				child2.channels.put(j, parent2.channels.get(j));
			}else{
				child2.channels.put(j, parent1.channels.get(j));
				child1.channels.put(j, parent2.channels.get(j));
			}
		}
		/*
		child1.channels = new HashMap<>();
		child2.channels = new HashMap<>();
		for(Map.Entry<String, Boolean> entry : parent1.conflicts.entrySet()) {
			String[] key = entry.getKey().split(" ");
			if(entry.getValue()){
				child1.channels.put(Integer.parseInt(key[0]), parent2.channels.get(Integer.parseInt(key[0])));
				child2.channels.put(Integer.parseInt(key[0]), parent1.channels.get(Integer.parseInt(key[0])));
			
				child1.channels.put(Integer.parseInt(key[1]), parent2.channels.get(Integer.parseInt(key[1])));
				child2.channels.put(Integer.parseInt(key[1]), parent1.channels.get(Integer.parseInt(key[1])));
			}else{
				child1.channels.put(Integer.parseInt(key[0]), parent1.channels.get(Integer.parseInt(key[0])));
				child2.channels.put(Integer.parseInt(key[0]), parent2.channels.get(Integer.parseInt(key[0])));
			}
		}*/
		child1 = new Fitness().calculate(child1);
		child2 = new Fitness().calculate(child2);
		child1.age = 0;
		child2.age = 0;
		if(child1.totalConflicts < child2.totalConflicts){
			return child1;
		}
		return child2;
	}
}
