package naturalMethod;

import java.util.concurrent.TimeUnit;

public class NaturalMethod {
	int conflictsWillBeConsider = 0;
	public NaturalMethod(ReadData data, 
			int availableChannel, 
			int availableUniqueChannel, 
			int populationSize){
		
		long startTime = System.currentTimeMillis();
		
		Population pop = new Population(data, 
				availableChannel, 
				availableUniqueChannel, 
				populationSize);
		pop.startGeneration();
		int generation = 0;
		while(true){
			if(pop.best.totalConflicts < conflictsWillBeConsider){
				System.out.println(pop.best);
				break;
			}
			//System.out.println(pop);
			System.out.println("Generation " + generation + ": " + pop.best);
			//pop.uniqueness(pop.best);
			pop.twinRemoval();
			pop.deathRace();
			pop.selfLearning();
			pop.collaboration();
			pop.crossOver();
			pop.increaseAge();
			
			generation++;
			
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println(getTime(totalTime) + "\n");
		}
		
		/*long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(getTime(totalTime));*/
	}

	public String getTime(long totalTime){
		if(totalTime > 100)
			return ("Time: " + String.format("%d min, %d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(totalTime),
			    TimeUnit.MILLISECONDS.toSeconds(totalTime) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))
			));
		else{
			return ("Time: " + totalTime + " ms");
		}
	}
	
	public static void main(String args[]){
		ReadData data = new ReadData("topology_800x800_tx120_cs301.txt");
		data.network = new Fitness().calculate(data.network);
		int availableChannel = 12;
		int availableUniqueChannel = 3;
		int populationSize = 50;
		new NaturalMethod(data, availableChannel, availableUniqueChannel, populationSize);
	}
}
