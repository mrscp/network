package naturalMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class Population extends Generator {
	Network best;
	
	public Population(ReadData data, 
			int availableChannel, 
			int availableUniqueChannel, 
			int populationSize) {
		this.availableChannel = availableChannel;
		this.availableUniqueChannel = availableUniqueChannel;
		this.populationSize = populationSize;
		best = data.network;
		this.conflictSchema = data.conflictSchema;
		this.linkSchema = data.linkSchema;
		this.links = data.links;
		nodes = data.nodes;
		initialConflicts = data.initialConflicts;
		totalLinks = data.totalLinks;
	}
	
	public void startGeneration(){
		for(int i = 0; i < populationSize; i++){
			population.add(i, randomNetwork(best));
			if(this.best.totalConflicts > population.get(i).totalConflicts){
				this.best = population.get(i);
			}
		}
	}
	
	public void selfLearning(){
		int count = 0;
		for(int i = 0; i < populationSize; i++){		
			Network temp = curingNeighbor(population.get(i));
			if(acceptance(population.get(i).totalConflicts, temp.totalConflicts) || temp.totalConflicts < population.get(i).totalConflicts){
				population.set(i,temp);
				count++;
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}
		}
		System.out.println("Learnt: " + count);
	}
	
	public void getTogether(){
		
	}
	
	public void collaboration(){
		for(int i = 0; i < populationSize; i++){		
			int x = selectAPerson();
			int y = selectAPerson();
			Network collaborate1[] = collaborate(population.get(x), population.get(y));
			population.set(x, collaborate1[0]);
			population.set(y, collaborate1[1]);
			Network collaborate2[] = collaborate(population.get(y), population.get(x));

			population.set(x, collaborate2[0]);
			population.set(y, collaborate2[1]);
		}
	}
	
	public void crossOver(){
		int count = 0;
		for(int i = 0; i < populationSize; i++){
			Network parent1 = population.get(selectAPerson());
			Network parent2 = population.get(selectAPerson());
			Network child = normalBirth(parent1, parent2);
			
			if(acceptance(population.get(i).totalConflicts, child.totalConflicts) || child.totalConflicts < population.get(i).totalConflicts){
				count++;
				population.set(i,child);
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}
		}
		System.out.println("crosses "+count);
	}
	
	public void increaseAge(){
		for(int i = 0; i < populationSize; i++){
			Network temp = population.get(i);
			temp.age++;
			population.set(i, temp);
		}
	}
	
	public void deathRace(){
		for(int i = 0; i < populationSize; i++){
			if(population.get(i).age > 10){
				population.set(i,randomNeighbor(best));
				System.out.println("death");
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}
		}
	}
	
	public void twinRemoval(){
		ArrayList<Network> tempPop = new ArrayList<>();
		for(int i = 0; i < populationSize; i++){
			if(tempPop.contains(population.get(i))){
				//System.out.println("twin removed");
				if(acceptance(best.totalConflicts, population.get(i).totalConflicts)){
					tempPop.add(randomNeighbor(best));
				}else{
					tempPop.add(randomNetwork(best));
				}
				
				if(this.best.totalConflicts > population.get(i).totalConflicts){
					this.best = population.get(i);
				}
			}else{
				tempPop.add(population.get(i));
			}
		}
		population = new ArrayList<>(tempPop);
	}
	
	public void uniqueness(Network network){
		for(Map.Entry<Integer, ArrayList<Integer>> entry: linkSchema.entrySet()){
			int count[] = new int[availableChannel];
			for(Integer target: entry.getValue()){
				int index = network.channels.get(links.get(generateLinkKey(entry.getKey(), target)));
				count[index]++;
			}
			System.out.println(entry.getKey() + "" + Arrays.toString(count));
		}
	}
	
	@Override
	public String toString() {
		String temp = "";
		for(Network network: population){
			temp += network + ",\n";
		}
		return "Population [" + temp + "]";
	}
}
